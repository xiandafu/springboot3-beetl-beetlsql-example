package org.example.beetlsql.mutiple.service;

import org.beetl.sql.core.SQLManager;
import org.example.beetlsql.mutiple.entity.DepartmentInfo;
import org.example.beetlsql.mutiple.mapper.db2.Db2Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Db2Service {
	@Autowired
	@Qualifier("sqlManager2")
	SQLManager sqlManager;

	@Autowired
	Db2Mapper deptMapper;


	@Transactional
	public DepartmentInfo queryDept(Integer id){

		return deptMapper.single(id);
		//or
//		return sqlManager.single(DepartmentInfo.class,id);

	}



}
