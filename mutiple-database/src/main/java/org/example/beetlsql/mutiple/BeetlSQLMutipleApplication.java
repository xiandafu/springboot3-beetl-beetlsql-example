package org.example.beetlsql.mutiple;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@PropertySource(value = {
        "classpath:application-two-db.yml"
}, encoding = "utf-8",factory =YamlPropertySourceFactory.class)
@SpringBootApplication
public class BeetlSQLMutipleApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeetlSQLMutipleApplication.class, args);
    }

}