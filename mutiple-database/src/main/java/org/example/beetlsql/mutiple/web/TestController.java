package org.example.beetlsql.mutiple.web;

import org.beetl.sql.core.SQLManager;
import org.example.beetlsql.mutiple.entity.DepartmentInfo;
import org.example.beetlsql.mutiple.entity.UserInfo;
import org.example.beetlsql.mutiple.service.Db2Service;
import org.example.beetlsql.mutiple.service.Db1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {
    @Autowired
    Db1Service db1Service;

    @Autowired
    Db2Service db2Service;

    @GetMapping("/")
    public String indexPage(){
        UserInfo userInfo = db1Service.queryUser(1);
        DepartmentInfo departmentInfo = db2Service.queryDept(1);
        return userInfo+"--"+departmentInfo;

    }
}
