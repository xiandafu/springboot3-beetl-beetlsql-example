package org.example.beetlsql.mutiple.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name="department")
public class DepartmentInfo {
	Integer id;
	String name;
}
