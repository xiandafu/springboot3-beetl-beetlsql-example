package org.example.beetlsql.mutiple.service;

import org.beetl.sql.core.SQLManager;
import org.example.beetlsql.mutiple.entity.UserInfo;
import org.example.beetlsql.mutiple.mapper.db1.Db1Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Db1Service {
	@Qualifier("sqlManager1")
	@Autowired
	SQLManager sqlManager;

	@Autowired
	Db1Mapper userInfoMapper;



	@Transactional
	public UserInfo queryUser(Integer id){
		return sqlManager.single(UserInfo.class,id);

	}


}
