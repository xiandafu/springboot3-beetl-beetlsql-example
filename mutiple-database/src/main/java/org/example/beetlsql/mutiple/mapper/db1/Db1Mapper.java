package org.example.beetlsql.mutiple.mapper.db1;

import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.example.beetlsql.mutiple.entity.UserInfo;


@SqlResource("user")
public interface Db1Mapper extends BaseMapper<UserInfo> {
	public UserInfo select();
}
