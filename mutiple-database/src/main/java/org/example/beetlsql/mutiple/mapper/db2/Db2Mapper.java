package org.example.beetlsql.mutiple.mapper.db2;

import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.example.beetlsql.mutiple.entity.DepartmentInfo;

@SqlResource("dept")
public interface Db2Mapper extends BaseMapper<DepartmentInfo> {
	public DepartmentInfo select();
}
