package org.beetl.sql.springboot.web;

import org.beetl.sql.springboot.entity.OrderLog;
import org.beetl.sql.springboot.service.OrderLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class 	TestController {

	@Autowired
	OrderLogService logService ;
	@GetMapping("/")
	public String indexPage(){
		List<OrderLog> tenantList = logService.logs(true,1);
		List<OrderLog> allList = logService.logs(false,1);
		return "teantList:"+tenantList+",allList:"+allList;

	}
	@GetMapping("/name")
	public String indexPage2(){
		List<OrderLog> tenantList = logService.queryByName(1,"a");
		return "tenantList:"+tenantList;

	}

	@GetMapping("/add")
	public OrderLog indexPage3(){
		OrderLog log = logService.add(2,"d");
		return log;

	}

	@GetMapping("/template")
	public List<OrderLog> indexPage4(){
		List<OrderLog> logs = logService.template(1,"a");
		return logs;

	}
}
