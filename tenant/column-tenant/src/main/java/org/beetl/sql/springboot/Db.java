package org.beetl.sql.springboot;

import java.util.function.Supplier;

/**
 * 切换租户id，高级做法有可以用AOP;beetlsql的自定义注解实现，比如
 * <pre>
 *
 *
 * @Tenant
 * Integer tenantId;
 *
 * </pre>
 *
 *
 * @see "DatasourceConfig.mySQLManagerCustomize"
 */
public class Db {
	public static ThreadLocal<Integer> localValue = ThreadLocal.withInitial(() -> null);

}
