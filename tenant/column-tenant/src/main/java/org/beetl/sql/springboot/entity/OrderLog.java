package org.beetl.sql.springboot.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.Auto;
import org.beetl.sql.annotation.entity.AutoID;

@Data
public class OrderLog {
	@AutoID
	Integer id;
	String name;
	Integer tenantId;
}
