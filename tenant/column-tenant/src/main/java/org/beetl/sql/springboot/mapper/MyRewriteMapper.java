package org.beetl.sql.springboot.mapper;

import com.beetl.sql.rewrite.annotation.DisableRewrite;
import com.beetl.sql.rewrite.mapper.RewriteBaseMapper;
import org.beetl.sql.mapper.annotation.AutoMapper;
import org.beetl.sql.mapper.internal.InsertAMI;
import org.beetl.sql.springboot.entity.OrderLog;

import java.util.List;

/**
 * 支持sql重写，会带上租户标记
 * @see RewriteBaseMapper
 */
public interface MyRewriteMapper extends RewriteBaseMapper<OrderLog> {
	List<OrderLog> select(String name);
	@DisableRewrite /*可以使用此禁止sql重写*/
	List<OrderLog> select2(String name);

	@AutoMapper(InsertAMI.class)
	int insert(OrderLog entity);

}
