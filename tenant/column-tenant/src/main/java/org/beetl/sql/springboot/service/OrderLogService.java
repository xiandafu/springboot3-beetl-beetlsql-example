package org.beetl.sql.springboot.service;

import org.beetl.sql.springboot.Db;
import org.beetl.sql.springboot.entity.OrderLog;
import org.beetl.sql.springboot.mapper.MyCommonMapper;
import org.beetl.sql.springboot.mapper.MyRewriteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderLogService {

	@Autowired
	MyCommonMapper myCommonMapper;

	@Autowired
	MyRewriteMapper myRewriteMapper;
	public List<OrderLog> logs(boolean isTenant,int tenantId){
		if(isTenant){
			Db.localValue.set(tenantId);
			return myRewriteMapper.all();
		}else{
			return myCommonMapper.all();
		}
	}

	public List<OrderLog> queryByName(int tenantId,String name){
		Db.localValue.set(tenantId);
		return myRewriteMapper.select(name);
	}

	public OrderLog add(int tenantId,String name){
		//对于插入，需要显示的设置tenantId。因为sql重写，只重写了where部分，insert语句没有where
		Db.localValue.set(tenantId);
		OrderLog log = new OrderLog();
		log.setName(name);
		log.setTenantId(tenantId);
		myRewriteMapper.insert(log);
		return log;
	}

	public List<OrderLog> template(int tenantId,String name){
		Db.localValue.set(tenantId);
		OrderLog log = new OrderLog();
		log.setName(name);
		return myRewriteMapper.template(log);
	}
}
