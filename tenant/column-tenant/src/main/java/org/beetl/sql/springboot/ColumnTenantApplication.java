package org.beetl.sql.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColumnTenantApplication {
	public static void main(String[] args) {
		SpringApplication.run(ColumnTenantApplication.class, args);
	}

}
