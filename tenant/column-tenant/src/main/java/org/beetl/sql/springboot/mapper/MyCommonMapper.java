package org.beetl.sql.springboot.mapper;

import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.springboot.entity.OrderLog;

import java.util.List;

/**
 * 普通
 */
public interface MyCommonMapper extends BaseMapper<OrderLog> {
	List<OrderLog> select(String name);


}
