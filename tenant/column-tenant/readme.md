适用于通过字段来区分多租户
需要配置如下，这样使用Interceptor方式，RewriteConfig会重写**sql的wehere部分**，为sql语句增加tenant_id过滤字段，其值自定义

```java
@Bean
public SQLManagerCustomize mySQLManagerCustomize() {
    return new SQLManagerCustomize() {
        @Override
        public void customize(String sqlMangerName, SQLManager manager) {
            DBInitHelper.executeSqlScript(manager, "db/schema.sql");
            manager.refresh();
            RewriteConfig rewriteConfig = new RewriteConfig();
            rewriteConfig.config(manager);
            rewriteConfig.addColRewriteConfig(new ColRewriteParam("tenant_id", new ColValueProvider() {
                @Override
                public Object getCurrentValue() {
                    return Db.localValue.get();
                }
            }));

        }
    };

}
```

* 访问http://127.0.0.1:8080/ 可以看到支持多租户和不支持多租户的查询结果
* http://127.0.0.1:8080/name  看到查询租户1的结果
* http://127.0.0.1:8080/add  插入一条数据

RewriteConfig的原理是重写SQL语句，他会解析SQL语句中的表，如果表中包含`tenant_id`字段，则再相应的的where语句部分加入tenant_id=xxx，这是通过
ColValueProvider来提供的值

对于没有where的语句，比如insert，RewriteConfig不会在insert语句中加入tenant_id，因此，程序还比如显示的设置tenant_id，比如
OrderLogService.add
```java
//对于插入，需要显示的设置tenantId。因为sql重写，只重写了where部分，insert语句没有where
Db.localValue.set(tenantId);//无效设置
OrderLog log = new OrderLog();
log.setName(name);
log.setTenantId(tenantId);
myRewriteMapper.insert(log); 
```



