package org.beetl.sql.springboot.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.Auto;
import org.beetl.sql.annotation.entity.AutoID;
import org.beetl.sql.annotation.entity.Table;

/**
 * 表名称变成动态的，带上了租户
 * @see "DatasourceConfig.tableTenantSQLManagerCustomize"
 */
@Data
@Table(name="${toTable('order_log')}")
public class OrderLog {
	@AutoID
	Integer id;
	String name;
}
