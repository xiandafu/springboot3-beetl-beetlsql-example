package org.beetl.sql.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TableTenantApplication {
	public static void main(String[] args) {
		SpringApplication.run(TableTenantApplication.class, args);
	}

}
