此代码实例了每一个租户一张表的情况，使用了”虚拟表技术实现“,参考代码DatasourceConfig.tableTenantSQLManagerCustomize
```java
manager.addVirtualTable("order_log","${toTable('order_log')}");
manager.refresh();

BeetlTemplateEngine templateEngine = (BeetlTemplateEngine)manager.getSqlTemplateEngine();
//表${toTable('order_log')}是个虚拟的，数据库定义来自order_log
manager.addVirtualTable("order_log","${toTable('order_log')}");
// 注册一个方法来实现映射到多表的逻辑
templateEngine.getBeetl().getGroupTemplate().registerFunction("toTable", new Function(){
    @Override
    public Object call(Object[] paras, Context ctx) {
        String tableName = (String)paras[0];
        //租户表
        return tableName+"_"+Db.localValue.get();

    }
});
```

这段代码指定了虚拟表`${toTable('order_log')}` 的meta数据来自实际表`order_log`.
toTable是一个普通的模板方法，当sql模板渲染的时候，将会调用此方法，重新生成租户表并返回。

除了这个配置外，你必须把原来的表改成`${toTable('order_log')}`,比如
```
@Data
@Table(name="${toTable('order_log')}")
public class OrderLog {
	@AutoID
	Integer id;
	String name;
}

```

或者SQL模板语句里
```markdown
select
===
* 多租户会重写此表
	select * from ${toTable("order_log")} o where name=#{name}
```

