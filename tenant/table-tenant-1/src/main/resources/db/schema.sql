DROP TABLE IF EXISTS `order_log`;
CREATE TABLE `order_log` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
       `name` varchar(12) ,
      PRIMARY KEY (`id`)
) ;

-- 俩个租户表1，2

DROP TABLE IF EXISTS `order_log_1`;
CREATE TABLE `order_log_1` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
       `name` varchar(12) ,
      PRIMARY KEY (`id`)
) ;

INSERT INTO `order_log_1` (id,name) VALUES (1, 'a');
INSERT INTO `order_log_1` (id,name)  VALUES (2,'b');

DROP TABLE IF EXISTS `order_log_2`;
CREATE TABLE `order_log_2` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
       `name` varchar(12) ,
      PRIMARY KEY (`id`)
) ;

INSERT INTO `order_log_2` (id,name) VALUES (1, 'e');
INSERT INTO `order_log_2` (id,name)  VALUES (2,'e');



COMMIT;
