package org.beetl.sql.springboot.service;

import org.beetl.sql.springboot.Db;
import org.beetl.sql.springboot.entity.OrderLog;
import org.beetl.sql.springboot.mapper.OrderLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderLogService {

	@Autowired
	OrderLogMapper orderLogMapper;


	public List<OrderLog> logs(int tenantId){
		Db.localValue.set(tenantId);
		return orderLogMapper.all();
	}
	public List<OrderLog> queryByName(int tenantId,String name){
		Db.localValue.set(tenantId);
		return orderLogMapper.select(name);
	}


	public OrderLog add(int tenantId,String name){
		Db.localValue.set(tenantId);
		OrderLog log = new OrderLog();
		log.setName(name);
		orderLogMapper.insert(log);
		return log;
	}

}
