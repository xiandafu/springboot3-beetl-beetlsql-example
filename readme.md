使用Beetl和BeetlSQL的例子

* simple 快速入门，使用说明参考 http://ibeetl.com/notes/quickstart/
* mutiple-database  工程使用多种数据库
* tenant 多租户
  * 单库单表多租户: 通过表的字段来区分多租户
  * 单库多表多租户：为每个租户建立一个表
  * 单库多用户的多租户: 使用数据库的用户来区分多租户，每个租户有有一个数据用户，物理隔离
  * 多库租户： 每个数据库一个租户，物理隔离
  * 多种数据库的租户：同多库租户，但数据库是不同的，比如mysql和oracle
