package org.example.beetl.web;

import jakarta.servlet.http.HttpServletRequest;
import org.beetl.core.BeetlKit;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class TestController {
    @GetMapping("/")
    public ModelAndView indexPage(){
        String name = "beetl";
        ModelAndView modelAndView  = new ModelAndView("index.html");
        modelAndView.addObject("name",name);
        return modelAndView;
    }
	@GetMapping("/script")
	@ResponseBody
	public Map script(){
		String script = "var a = 5;var b = 3+a; return b;";
		Map map = BeetlKit.execute(script);
		return map;
	}
}
