package org.example.beetl;


import org.beetl.ext.spring6.EnableBeetl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBeetl
public class BeetlApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeetlApplication.class, args);
    }

}