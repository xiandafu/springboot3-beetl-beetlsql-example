package org.example.beetlsql.web;

import jakarta.servlet.http.HttpServletRequest;
import org.beetl.sql.core.SQLManager;
import org.example.beetlsql.entity.Book;
import org.example.beetlsql.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class TestController {
    @Autowired
    BookService bookService;

    @Autowired
    SQLManager sqlManager;

    @GetMapping("/")
    public ModelAndView indexPage(){
        String name = "lijz";
        List<Book> list = bookService.selectBook(name);
        ModelAndView modelAndView  = new ModelAndView("index.html");
        modelAndView.addObject("books",list);
        return modelAndView;
    }
}
