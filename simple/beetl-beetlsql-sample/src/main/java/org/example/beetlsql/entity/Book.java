package org.example.beetlsql.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.AutoID;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name="book")
public class Book {
    @AutoID
    private Integer id;
    private String name;
}
