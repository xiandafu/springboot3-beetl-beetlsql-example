package org.example.beetlsql.service;

import org.example.beetlsql.entity.Book;
import org.example.beetlsql.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookService {
    @Autowired
    BookMapper bookMapper;
    public List<Book> selectBook(String name){
        Book book = new Book();
        book.setName(name);
        bookMapper.insert(book);
        return bookMapper.queryByName(name);
    }
}
