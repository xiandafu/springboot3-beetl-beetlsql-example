package org.example.beetlsql.mapper;

import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.example.beetlsql.entity.Book;

import java.util.List;

@SqlResource("book1")
public interface BookMapper extends BaseMapper<Book> {
    List<Book> queryByName(String name);
}
