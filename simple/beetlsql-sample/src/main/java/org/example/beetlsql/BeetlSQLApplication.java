package org.example.beetlsql;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeetlSQLApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeetlSQLApplication.class, args);
    }

}