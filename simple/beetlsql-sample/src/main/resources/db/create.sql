DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ;

BEGIN;
INSERT INTO `book` VALUES (1, 'lijz');
INSERT INTO `book` VALUES (2, 'lucy');
INSERT INTO `book` VALUES (3, 'bear');
COMMIT;
